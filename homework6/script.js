const mainBlock = document.querySelector(".main-block")
let postCounter=100;

        window.onload = async function () {
            document.getElementById('fountainG').remove()
            await fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json()).then((data) => {
                data.forEach(function (item) {
                    const {userId, id, title, body} = item;
                    mainBlock.insertAdjacentHTML("afterbegin", `<div class="card card-${userId}" id=${id} >  <button class="edit-btn btn btn-warning" id="edit-btn-${id}">Edit</button><button class="delete-btn btn btn-danger" id="delete-btn-${id}">Delete</button>  <span class="title" id="title_${id}">${title}</span>  <div id="content_${id}" class="card-content">${body}</div> </div>`)

                    document.querySelector(`#delete-btn-${id}`).addEventListener("click", async function deleteCardFunc(ev) {
                            await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
                                method: "DELETE"
                            }).then(function(response){
                                if(response.ok){
                                    ev.target.parentElement.remove()
                                    postCounter--
                                    console.log(postCounter)
                                }
                            })
                        
                        })
                   

                    document.querySelectorAll(".edit-btn").forEach(function (item) {
                        item.addEventListener("click", function (ev) {
                            let title = document.getElementById(`title_${id}`)
                            title.innerHTML = `<input class="edit-title-input" id="edit-title-input-${id}">`
                            let content = document.getElementById(`content_${id}`)
                            content.innerHTML = `<input class="edit-content-input" id="edit-content-input-${id}">`
                            content.insertAdjacentHTML("afterend", `<input type="submit" class="edit-submit btn btn-success" id="edit-submit-${id}">`)
                            document.getElementById(`edit-submit-${id}`).addEventListener("click", async function (ev) {

                                await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
                                    method: 'PUT',
                                    data: `title=${title}&body=${content}`,
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify(data)
                                }).then((response) => response.json()).then(function (data) {
                                    title.innerText = document.querySelector(`#edit-title-input-${data.id}`).value;
                                    content.innerText = document.querySelector(`#edit-content-input-${data.id}`).value;
                                    document.getElementById(`edit-submit-${id}`).remove();
                                })
                            })


                        })
                    })
                })
            })
            

            await fetch("https://jsonplaceholder.typicode.com/users").then((response) => response.json()).then((data) => {
                data.forEach(function (item) {
                    const {username, id, name, email} = item
                    document.querySelectorAll(`.card-${id}`).forEach(function (item) {
                        item.insertAdjacentHTML("afterbegin", `<span class="userinfo">${username} &nbsp; ${email} &nbsp; ${name}</span>`)
                    })
                })
            })
        
        document.getElementById("add-new-post").addEventListener("click", newPostModalRender)

        function newPostModalRender() {
            const modalWindow = document.querySelector("#modal-window")
            modalWindow.style.display = 'flex';
            modalWindow.insertAdjacentHTML("afterbegin", '<input  id="title-input-modal" placeholder="enter title"> <input id="body-input-modal" placeholder="enter text content"> <input type="submit" class="btn btn-success" id="createNewCardBtn"> <span id="closeModalCross">&times</span> ');
            document.querySelector("#createNewCardBtn").addEventListener("click", createNewCard);
            document.querySelector("#closeModalCross").addEventListener("click", closeModal);
        }

       async function createNewCard(){
          const request = await fetch('https://jsonplaceholder.typicode.com/posts', {
               method:"POST",
               body:JSON.stringify({
                   id:(Math.floor(Math.random*1000)),
                   title:document.getElementById("title-input-modal").value,
                   body:document.getElementById("body-input-modal").value,
                   userId:1,
               }),
               headers: {
                "Content-type": "application/json; charset=UTF-8"
              }
        
           })
           if(request.ok){
                let title = document.getElementById("title-input-modal").value;
                let body = document.getElementById("body-input-modal").value;
              

               closeModal()
               postCounter++
               mainBlock.insertAdjacentHTML("afterbegin", `<div class="card card-${1}" id=${postCounter}>  <span class="userinfo"> user &nbsp; user@gmail.com &nbsp</span> <button class="edit-btn btn btn-warning" id="edit-btn-${postCounter}">Edit</button><button class="delete-btn btn btn-danger" id="delete-btn-${postCounter}">Delete</button>  <span class="title" id="title_${postCounter}">${title}</span>  <div id="content_${postCounter}" class="card-content">${body}</div> </div>`)
               document.getElementById(`delete-btn-${postCounter}`).addEventListener("click", async function deleteCardFunc(ev) {
                await fetch(`https://jsonplaceholder.typicode.com/posts/${postCounter}`, {
                    method: "DELETE"
                }).then(function(response){
                    if(response.ok){
                        ev.target.parentElement.remove()
                        postCounter--
                    }
                })
            
            })
            document.getElementById(`edit-btn-${postCounter}`).addEventListener("click", function(ev){
                let title = document.getElementById(`title_${postCounter}`)
                title.innerHTML = `<input class="edit-title-input" id="edit-title-input-${postCounter}">`
                let content = document.getElementById(`content_${postCounter}`)
                content.innerHTML = `<input class="edit-content-input" id="edit-content-input-${postCounter}">`
                content.insertAdjacentHTML("afterend", `<input type="submit" class="edit-submit btn btn-success" id="edit-submit-${postCounter}">`)
                document.getElementById(`edit-submit-${postCounter}`).addEventListener("click", async function(ev){
                    const request = await fetch(`https://jsonplaceholder.typicode.com/posts/${postCounter}`, {
                        method:'PUT',
                        data: `title=${title}&body=${content}`,
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(this.data)
                    })
                    title.innerText = document.querySelector(`#edit-title-input-${postCounter}`).value;
                    content.innerText = document.querySelector(`#edit-content-input-${postCounter}`).value;
                    document.getElementById(`edit-submit-${postCounter}`).remove();
                   
                })
            } )
           
           }
       }

        function closeModal() {
            document.querySelector("#modal-window").style.display = 'none';
            document.querySelector("#body-input-modal").remove();
            document.querySelector("#title-input-modal").remove();
            document.querySelector("#createNewCardBtn").remove();
            document.querySelector("#closeModalCross").remove();
        }
    }

