import React, {useState} from "react";
import {Link, BrowserRouter as Router} from "react-router-dom"
import "./Header.css"

const Header = (props) => {

    const [activeLi,setActiveLi] = useState({
        home:false,
        humans:false,
        clothes:false,
        weapons:false
    })

    function switchStripe(name){
        switch (name) {
            case "humans":setActiveLi({humans:true, clothes:false, weapons:false , home:false})
                break;
            case "clothes":setActiveLi({humans:false,clothes:true,weapons:false , home:false})
                break;
            case "weapons":setActiveLi({humans:false,clothes:false,weapons:true, home:false})  
                 break;
            case "home":setActiveLi({humans:false, clothes:false,weapons:false, home:true})     
            default:
                break;
        }
    }
    const {cart, openModal} = props;
    let quantity = 0;
    cart.forEach((item)=>{
        quantity = quantity + item.quantity
    })
    return(
    
        <header className="header container-fluid"> 
               <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand">Super-shop!</a>
       
        <div className="navbar row" id="navbarNav">
            
            <ul className="navbar-nav col-12">
            <li onClick={()=>switchStripe("home")} className={activeLi.home?"nav-active nav-item":"nav-item"} >
                <Link to="/" className="nav-link">Home </Link>
            </li>
            <li onClick={()=>switchStripe("humans")} className={activeLi.humans?"nav-active nav-item":"nav-item"} >
                <Link to="/humans" className="nav-link">Humans </Link>
            </li>
            <li onClick={()=>switchStripe("clothes")} className={activeLi.clothes?"nav-active nav-item":"nav-item"}>
                <Link to="/clothes" className="nav-link" >Clothes</Link>
            </li>
            <li  onClick={()=>switchStripe("weapons")} className={activeLi.weapons?"nav-active nav-item":"nav-item"}>
                <Link to="/weapons" className="nav-link" >Weapons</Link>
            </li>
        
            <li className="nav-item" onClick={openModal}>
                <div className="cart-btn"></div>
            </li>
            <div className="cart-counter">{quantity}</div>
            </ul>
      
    </div>
    </nav>
            
        </header>   
    )
}
export default Header