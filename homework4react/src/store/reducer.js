const reducer = (state,action) => {
        switch (action.type) {
            case  "START_TIMER":
               {
                   let newSecs = +state.seconds;
                   let newTens = +state.tens+1;
                   if (newTens<9){
                       newTens="0"+newTens;

                   }
                   if (newTens>99){
                       newTens=0;
                       newSecs++
                   }
                   return { tens:newTens, seconds:newSecs}

               }
            case "RESET_TIMER":{
                return {seconds: 0, tens: 0}
            }
            default :
                console.log("not lol")
        }
};
export default reducer;