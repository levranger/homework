class Hamburger {
    constructor(size, stuffing) {
        if (size.name === `SIZE_SMALL` || size.name === 'SIZE_LARGE') {
            this.size = size;
        } else throw new Error('Size is wrong');

        if (stuffing.name === 'STUFFING_CHEESE' || stuffing.name === 'STUFFING_SALAD' || stuffing.name === `STUFFING_POTATO`) {
            this.stuffing = stuffing
        } else throw new Error('Stuffing is wrong');

        this.toppings = [];
    };
    addTopping(toppingObject){
        if (toppingObject.name === 'TOPPING_MAYO'||toppingObject.name === 'TOPPING_SPECIES'||!this.toppings.includes(toppingObject)){
            this.toppings.push(toppingObject)
        }
        else throw new Error('Topping is wrong')
    };
    removeTopping(toppingObject) {
        if (this.toppings.includes(toppingObject)) {
            this.toppings.splice(toppingObject.index, 1)
        } else throw new Error('Something went wrong')
    }
    calculatePrice(object){
        let totalPrice = 0;
        let toppingPrice = 0;
      totalPrice =  totalPrice + object.size.price + object.stuffing.price ;
        object.toppings.forEach(item => toppingPrice + item.price)
        return totalPrice + toppingPrice

    }
    calculateCalories(object){
        let totalCalories = 0;
        let toppingCalories = 0;
       totalCalories = totalCalories + object.size.calories + object.stuffing.calories;
       object.toppings.forEach(item => toppingCalories + item.calories )
        return totalCalories + toppingCalories
    }




}
let hamburger1 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.TOPPING_SPECIES);
hamburger1.addTopping(Hamburger.TOPPING_SPECIES);


Hamburger.SIZE_SMALL = {
    name:`SIZE_SMALL`,
    price:50,
    calories:20,
};
Hamburger.SIZE_LARGE = {
    name:`SIZE_LARGE`,
    price:100,
    calories:40
};
Hamburger.STUFFING_CHEESE = {
    name:`STUFFING_CHEESE`,
    price:10,
    calories:20
};
Hamburger.STUFFING_SALAD = {
    name:`STUFFING_SALAD`,
    price:10,
    calories:5
};
Hamburger.STUFFING_POTATO = {
    name:`STUFFING_POTATO`,
    price:15,
    calories:10
};
Hamburger.TOPPING_MAYO = {
    name:`TOPPING_MAYO`,
    price:20,
    calories:5,
};
Hamburger.TOPPING_SPECIES = {
    name:`TOPPING_SPECIES`,
    price:15,
    calories:0
};
