import "./Footer.css";

const Footer = () => {
        return(
            <footer className="footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 footer-content">
                                <span>Super-shop!</span>
                               <div className="contact"><p>Please dont try to contact us, you will regret if you call this number <br/> <strong>+666 - 666 - 666 - 666</strong> </p></div> 
                                <strong className="rights">None of the rights are reserved</strong>
                            </div>
                        </div>
                    </div>
            </footer>
        )
};

export default Footer;