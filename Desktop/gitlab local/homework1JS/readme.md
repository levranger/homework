Объявление переменной через var и let.
Переменная объявленная через var видна до и
после её объявления, при попытке обращения к ней до её объвления
выдаст undefined, в то время как обращение через let до её объявления выдаст ошибку, именно поэтому объявление переменной через
var считается дурным тоном, и вместо неё используется let.

Константа, она же const не подлежит изменению, при попытке будет выдавать ошибку.