import React from "react";
import "./Footer.css"
import Logo from "../../shared/Logo/Logo";
import FooterLogo from  './FooterLogo.png'
const Footer = () => {
    return(
        <div className={'footer'}>
            <div className={"footer-nav"}>
                <span>About  </span>
                <span>Terms of service </span>
                <span>Contact </span>
                <Logo src={FooterLogo} alt={"Footer logo"} className={"footer-logo"}/>
            </div>
            <span className={"copyright-footer"}>Copyright © 2017 MOVIERISE. All Rights Reserved.</span>
        </div>
    )
};
export default Footer