import React, {Component} from "react";
import "./Logo.css";
class Logo extends Component{
    constructor(props) {
        super(props);
    }
    render() {
        const {src, alt, className} = this.props;
        return(
            <img src={src} alt={alt} className={className}/>
        )
    }
}
export default Logo;
