import React,{useState, useReducer} from 'react';
import './App.css';
import reducer from "./store/reducer"
import initialState from "./store/initialState";
let interval;
function App() {

const startTimer = () => {
    interval = setInterval(()=>dispatch({type:"START_TIMER"}),15)
};
const stopTimer = () => {
    clearInterval(interval)
};
const resetTimer = () => {
    clearInterval(interval)
    dispatch({type:"RESET_TIMER"})
};
 const [state,dispatch] = useReducer(reducer,initialState);

  return (
    <div className="App">
        <button onClick={startTimer}>Start</button>
        <button onClick={stopTimer}>Stop</button>
        <button onClick={resetTimer}>Reset</button>
        <span>{state.seconds}:{state.tens}</span>
    </div>
  )
}

export default App;
