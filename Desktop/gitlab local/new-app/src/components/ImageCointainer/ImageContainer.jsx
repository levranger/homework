import React, { useState , useEffect } from 'react';
import "./ImageContainer.css";

const ImageContainer = (props) => {
    const [images, setImages] = useState([]);
    const {imagesArr} = props;
    console.log(images, imagesArr )
    console.log(["Image Container Render"])
    useEffect(()=>{
       setImages(imagesArr)
      }, [imagesArr])

    const imageItems = images.map(({thumbnail})=> {

     return  <img className="col-lg-3 col-md-4 col-xs-12 image-card "  src={`https://uwatch.live${thumbnail}`}/>
       
      

    })

    return(
        <div className="image-container container">
                <div className="row">
                        {imageItems}
                </div>
        </div>
    )


}
export default ImageContainer;
