import React, { useState , useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import ImageContainer from './components/ImageCointainer/ImageContainer';
import { useQuery, gql } from '@apollo/client';
  

const GET_MOVIES = gql`
query getPostList($input: PostSearchType) {
    posts(input: $input , paging : {limit:12}) {
      items{
      thumbnail
      }
   
    }
  }
  
`

function App() {
  const [images, setImages] = useState([]);
  const { loading, data, error } = useQuery(GET_MOVIES , {"input":{"type":"post","locale":"en","projectId":"1"}})
  useEffect(()=>{
    if(data) setImages(data.posts.items)
  }, [data])

  return (
    <div className="App">
          <ImageContainer imagesArr={images}/>
    </div>
  );
 
}

export default App;
