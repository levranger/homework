function Hamburger(size, stuffing) {
    try {
        if (size.name === `SIZE_SMALL`||size.name === `SIZE_LARGE`){
            this.size = size
        }
        else  new Error(`invalid size`)
    }
    catch (e) {
        throw new Error(`invalid size`)
    }


    try{
        if (stuffing.name === `STUFFING_CHEESE`||stuffing.name === `STUFFING_SALAD`||stuffing.name === `STUFFING_POTATO`){
            this.stuffing = stuffing
        }
        else new Error(`invalid stuffing`)
    }
    catch (e) {
        throw new Error(`invalid stuffing`)
    }

    this.toppings = []


}
Hamburger.SIZE_SMALL = {
    name:`SIZE_SMALL`,
    price:50,
    calories:20,
};
Hamburger.SIZE_LARGE = {
    name:`SIZE_LARGE`,
    price:100,
    calories:40
};
Hamburger.STUFFING_CHEESE = {
    name:`STUFFING_CHEESE`,
    price:10,
    calories:20
};
Hamburger.STUFFING_SALAD = {
    name:`STUFFING_SALAD`,
    price:10,
    calories:5
};
Hamburger.STUFFING_POTATO = {
    name:`STUFFING_POTATO`,
    price:15,
    calories:10
};
Hamburger.TOPPING_MAYO = {
    name:`TOPPING_MAYO`,
    price:20,
    calories:5,
};
Hamburger.TOPPING_SPECIES = {
    name:`TOPPING_SPECIES`,
    price:15,
    calories:0
};

Hamburger.prototype.addTopping = function(objectTopping){
    if (this.toppings.includes(objectTopping)){
        throw new Error(`This topping has been added`)
    }
    if(objectTopping.name === `TOPPING_MAYO`||objectTopping.name === `TOPPING_SPECIES`){
        this.toppings.push(objectTopping)
    }
    else throw new Error(`invalid topping`)
};
Hamburger.prototype.deleteTopping = function(objectTopping){
    if (this.toppings.includes(objectTopping)){
        this.toppings.splice(objectTopping.index, 1)
        console.log(`topping has been deleted`)
    }
    else throw new Error(`This topping has not been added`)
};
Hamburger.prototype.calculatePrice = function(){
    let totalPrice = 0;
    let toppingPrice = 0;
    totalPrice = totalPrice + this.size.price + this.stuffing.price ;
    this.toppings.forEach(item => toppingPrice = item.price + toppingPrice );
    totalPrice = toppingPrice + totalPrice
    return totalPrice
};

Hamburger.prototype.calculateCalories = function(){
    let totalCalories = 0;
    let toppingCalories = 0;
    totalCalories =+ this.size.calories + this.stuffing.calories
    this.toppings.forEach(item => toppingCalories = toppingCalories + item.calories)
    totalCalories = totalCalories + toppingCalories
    console.log(totalCalories)
    // return totalCalories
}
Hamburger.prototype.getToppings = function(){
    this.toppings.forEach(item => console.log(item.name))
}
Hamburger.prototype.getSize = function(){
    return this.size.name
}




let hamburger1 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger1.addTopping(Hamburger.TOPPING_MAYO);


console.log(hamburger1);


