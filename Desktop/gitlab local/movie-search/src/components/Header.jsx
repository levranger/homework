import React from "react";
import "./Header.css";
const Header = (props) => {
    const {title} = props;
   return(
       <header className="header">
           {title}
       </header>
   ) 
}

export default Header