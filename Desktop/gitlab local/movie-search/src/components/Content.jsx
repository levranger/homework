import React, { useEffect } from "react";
import "./Content.css"
const Content = (props) => {
    const {movies} = props;

   let movieCards =  movies.map((item)=>{
  return <div className="movie-card col-4">
        <div className="movie-label">
              <h1>{item.Title}</h1>
          
        </div>
        <img src={item.Poster} alt=""/>
        <span className="movie-year">{item.Year}</span>
    </div>
   } );

        return( 
        <div className="content container">
            <div className= "row">
            {movieCards}
            </div>
        </div>)

}
export default Content;