import React from "react";

const ProductCart = (props) => {
    const {dispatch,cartItems} = props;
   let totalPrice = 0;
   cartItems.forEach((item)=>{
       if (item.quantity===1){
           totalPrice+=item.price
       } else {
           let sum = item.quantity*item.price;
           totalPrice+=sum
       }
   });
    return(<ul> <button onClick={()=>dispatch({type:"DELETE_ALL_PRODUCTS"})}>Clear cart</button>
        <p>total price : {totalPrice}</p>
        {cartItems.map(({name,price, id , quantity})=> <li key={id}>{name}, {price}$  |  {quantity}  <button onClick={()=>dispatch({type:"INCREASE_QUANTITY", payload:{name,quantity}})}>+</button> <button onClick={()=>dispatch({type:"DECREASE_QUANTITY", payload:{name,quantity,id,price}})}>-</button></li>)}
    </ul>)
};

export default ProductCart;