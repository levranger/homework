import React,{useReducer} from 'react';
import './App.css';
import ProductList from "./components/ProductList/ProductList";
import ProductCart from "./components/ProductCart/ProductCart";
const productList =[
    {name:"Wine", price:100, quantity:1, id:1},
    {name:"Beer", price: 50, quantity: 1, id:2},
    {name:"Vodka", price:70, quantity:1, id:3}
];
const cart = {
        totalPrice:0,
        cartItems:[]
};


const App = () => {
    const [state, dispatch] = useReducer(reducer, cart);
    function reducer(state, action) {
        switch (action.type) {
            case "ADD_TO_CART":
                console.log({...action.payload});
                let found = state.cartItems.some((item)=>item.name ===action.payload.name);
                console.log(found);
                if(!found) {
                    return {
                        ...state,
                        cartItems: [...state.cartItems, {...action.payload}]
                    }
                } else {
                    const newProductList = state.cartItems.map((item)=>{
                        if (item.name===action.payload.name){
                            return {
                                ...item,
                                quantity:item.quantity+1
                            }; } else return {
                                ...item
                            }

                    });
                    return {
                        ...state,
                        cartItems: newProductList
                    }
                }
            case "INCREASE_QUANTITY":
                const newProductList = state.cartItems.map(item=>{
                    if (item.name===action.payload.name){
                        return {
                            ...item,
                            quantity:item.quantity+1
                        }
                    } else return item;
                });
                return {
                    ...state,
                    cartItems:newProductList
                };
            case "DECREASE_QUANTITY":
                if (action.payload.quantity>1) {
                    const newCartItems = state.cartItems.map(item => {
                        if (item.name === action.payload.name) {
                            return {
                                ...item,
                                quantity: item.quantity - 1
                            }
                        } else return item;


                    });
                    return {
                        ...state,
                        cartItems: newCartItems
                    };
                } else {
                    const idx = state.cartItems.findIndex(item=>item.name===action.payload.name);
                    const newArr = state.cartItems.splice(idx,1);
                    return {...state,
                            cartItems: newArr
                    }

                }

            case "DELETE_ALL_PRODUCTS":
                return {
                  ...state,
                  cartItems: []
                };
            default:return {...state};
    }

    }
    const productItems = productList.map(({name,price,id,quantity})=><li key={id}>{name} price:{price} {id} <button onClick={()=>dispatch({type:"ADD_TO_CART", payload:{name,price,id,quantity}})}>Add to cart</button></li>);

    return(
        <>
            <ProductList productItems={productItems}/>
            <button onClick={()=>console.log(cart)}>Lol</button>
            <ProductCart dispatch={dispatch} cartItems={state.cartItems}/>
        </>
    );

};

export default App;
