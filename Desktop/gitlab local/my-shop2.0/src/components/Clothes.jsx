import React from "react";
import "./Clothes.css";

const Clothes = (props) => {
    const {products, addToCart} = props;
    console.log(props)
    const productCards = products.map((item)=>{
        
        return  <div className="col-3 col-xs-12 product-card clothes-card">
                     <img src={item.img} alt=""/>
                      <h1>{item.name}</h1>
                     <p>Size : {item.size}</p>
                     <button className="btn btn-success" onClick={()=>addToCart(item)}>Buy</button>
                     <span>{item.price}$</span>

           </div>
    })
    
    return(
        <>
        <h1>Clothes</h1>
       <div className="row  product-row">
            {productCards}
        </div>
        </>

    )
}
export default Clothes;