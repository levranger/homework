import React from "react"

const Weapons = (props) => {
    const {products , addToCart} = props;
    const productCards = products.map((item)=>{
    
        return  <div className="col-3 col-xs-12 product-card">
                     <img src={item.img} alt=""/>
                      <h1>{item.name}</h1>
                     <p>Caliber {item.caliber}</p>
                     <button className="btn btn-success" onClick={()=>addToCart(item)}>Buy</button>
                     <span>{item.price}$</span>

           </div>
    })
    return(
        <>
        <h1>Weapons</h1>
       <div className="row  product-row">
            {productCards}
        </div>
        </>
    )
}
export default Weapons;