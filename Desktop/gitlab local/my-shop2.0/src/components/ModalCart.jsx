import React, {useState} from "react";
import "./ModalCart.css";
const ModalCart = (props) => {
    const {isOpened , closeModal, cart, increaseQuantity, decreaseQuantity, deleteItem, clearCart} = props;
    let totalPrice = 0;
    cart.forEach((item)=>{
        totalPrice += item.quantity * item.price
    })
    let quantity = 0;
    cart.forEach(item=>{
        quantity = quantity + item.quantity
    })
    const cartItems = cart.map((item)=>{
     return   <div className="col-12 cart-item">
            <h2>{item.name}  {item.size||item.caliber||item.age}</h2> <span>{item.quantity} X  {item.price}$</span>    
            <div className="buttons">
               <button onClick={()=>increaseQuantity(item)} className="btn btn-success">+</button> <button onClick={()=>decreaseQuantity(item)} className="btn btn-danger">-</button> <button onClick={()=>deleteItem(item)} className="btn btn-danger ">Delete</button>
             </div> 
        </div>
    })
    return(
        <div className={isOpened?"modal-cart visible container":"modal-cart invisible container"}>
            <div className="wrapper">
                    <div className="cross" onClick={closeModal}>X</div>
                    <h1 className="cart-label" >Your cart ({quantity}) </h1> <button onClick={clearCart} className="btn btn-danger  clear-cart-btn">Clear cart</button>
                    <div className="row ">
                        {cartItems}
                    </div>
                    <div className={quantity?"cart-lower-section visible":"cart-lower-section invisible"}>
                          <h4>TOTAL : <strong>{totalPrice}$</strong></h4>
                            <button onClick={()=>{ alert("Thank you for your order, actually i dont know backend so it's not complete online shop))") }} className="btn btn-success btn-pay">Pay!</button>
                    </div>
              </div>
        </div>
    )
}

export default ModalCart