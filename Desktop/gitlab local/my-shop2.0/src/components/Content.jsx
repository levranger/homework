import React from "react";
import Humans from "./Humans"
import Clothes from "./Clothes"
import Weapons from "./Weapons";
import HomePage from "./HomePage";
import products from "../shared/products";
import "./Content.css"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import "./Content.css"
const Content = (props) => {
  const {addToCart} = props;
    return(
    <div className="content">
          <h2>Our products</h2>
          <div className="products-container container">
            <div className="row">
              <div className="col-12 white-stripe"></div>
            </div>
                <Route exact path="/" component={()=> <HomePage /> }/>
                <Route path="/humans" component={()=><Humans products={products.humans} addToCart={addToCart}/>}/>
                <Route path="/clothes" component={()=><Clothes products={products.clothes}  addToCart={addToCart}/>}/>
                <Route path="/weapons" component={()=><Weapons products={products.weapons} addToCart={addToCart}/>}/>
             
           </div>     
  
    </div>)
    
}
export default Content