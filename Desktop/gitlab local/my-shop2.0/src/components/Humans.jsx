import React from "react"
import "./Humans.css"
const Humans = (props) => {
    const {products, addToCart} = props;
    

    const productCards = products.map((item)=>{
    
        return  <div className="col-3 col-xs-12 product-card">
                     <img src={item.img} alt=""/>
                      <h1>{item.name}</h1>
                      <p>Age : {item.age}</p>
                    
                     <button className="btn btn-success" onClick={()=>addToCart(item)} >Buy</button>
                     <span>{item.price}$</span>

           </div>
    })
    return(
        <>
        <h1>Humans</h1>
       <div className="row  product-row">
            {productCards}
        </div>
        </>
       
    )
}
export default Humans;