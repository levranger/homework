import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/Header";
import ModalCart from "./components/ModalCart";
import React, {useState} from "react";
import Content from "./components/Content";
import Footer from "./components/Footer";
import Header2 from "./components/Header2";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
function App() {
  const [cart,setCart]= useState([]);
  const [modal,setModal] = useState(false);
  const openModal = () => {
        setModal(true)
  }
  const closeModal = () => {
    setModal(false)
  }
  const clearCart = () => {
    setCart([]);
  }
  const deleteItem = (product) => {
    const newCart = cart.filter(item=>item.name !== product.name);
    setCart(newCart);
  }
  const increaseQuantity = (product) => {
    const newCart = cart.map(item=>{
      if(item.name === product.name){
        item.quantity++
        return item
      } else return item
    })
    setCart(newCart)
  }
  const decreaseQuantity = (product) => {
    if(product.quantity>1){
      const newCart = cart.map(item=> {
        if(item.name === product.name){
          item.quantity--
          return item;
        } else {
          return item
        }
      })
      setCart(newCart);
    } else {
      const newCart = cart.filter(item=>item.name !== product.name);
      setCart(newCart);
    }
  }
  const addToCart = (product) => {
        let match = cart.find((item)=>item.name===product.name);
        if(!match){
          setCart([...cart, {...product}])
        } else {
          const newCart = cart.map((item)=>{
            if(item.name===product.name){
              item.quantity++
              return item
            } else {
              return item
            }
          })
          setCart(newCart)
        }
  }
  return (
    <Router>
     <div className="App">
       <Header2 cart={cart} openModal={openModal}/>
        <Content addToCart={addToCart}/>
        <ModalCart cart={cart} isOpened={modal} closeModal={closeModal} clearCart={clearCart} deleteItem={deleteItem} decreaseQuantity={decreaseQuantity} increaseQuantity={increaseQuantity}/>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;
