import React,{Component} from "react";
import "./style.css";
import "./bootstrap.min.css"
import Header from  "./components/Header/index"
import Form from  "./components/Form/index"
import BookList from "./components/BooksList/index"
import {findAllInRenderedTree} from "react-dom/test-utils";
class App extends Component{
    state = {
        newBook:{},
        books:[{name:"Песнь Льда и Пламени", author:"Джордж Мартин", isbn:"0000111"}
        ]
    };
    deleteBook = (isbn) => {
        const ArrCopy = this.state.books;
       const newArr = ArrCopy.filter((item)=>item.isbn !== isbn);
        this.setState({books:newArr});
    };
    addBook = (event, {...props}) => {
        event.preventDefault();
        const {name,author,isbn} = props;
        const newItem = {name, author, isbn};
        const newArr = this.state.books.slice();
        newArr.push(newItem);
        this.setState({books:newArr})
    };
    updateBook= (event, {...props}) => {
        event.preventDefault();
        const {name,author,isbn, previousIsbn} = props;
        const newArr = this.state.books.slice();
        const index = newArr.findIndex((item)=>item.isbn===previousIsbn);
        newArr[index]={name,author,isbn};
        this.setState({books:newArr})
    };

    render() {
        return (
            <div className="container mt-4">
                <Header/>
                <Form addBook={this.addBook}/>
                <BookList books={this.state.books} deleteBook={this.deleteBook} bookCounter={this.state.books.length} updateBook={this.updateBook}/>
            </div>
        );
    }
}

export default App;
