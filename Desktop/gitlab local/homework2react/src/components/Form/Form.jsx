import React,{Component} from "react";
class Form extends Component{
    constructor(props) {
        super(props);
        this.state= {name:'', author:'', isbn:""};
    }
    render() {
        const {name, author, isbn} = this.state;

        return <div className="row">
            <div className="col-lg-4">
                <form id="add-book-form">
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input onChange={(event)=>this.setState({name:event.target.value})} type="text" name="book-name" className="form-control" value={this.state.name}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="author">Author</label>
                        <input  onChange={(event)=>this.setState({author:event.target.value})}  type="text" name="book-author" className="form-control" value={this.state.author}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="title">ISBN#</label>
                        <input  onChange={(event)=>this.setState({isbn:event.target.value})}  type="text" name="book-isbn" className="form-control" value={this.state.isbn}/>
                    </div>
                    <input onClick={(event)=>{this.setState({name:'', author:'', isbn:""}); this.props.addBook(event, {name,author,isbn})}} type={"submit"} className="btn btn-primary"/>
                </form>
            </div>
        </div>
    }}
export default Form;
