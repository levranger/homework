import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBookOpen} from "@fortawesome/free-solid-svg-icons/faBookOpen";

const Header = ()=>{
    return<>
        <h1 className="display-4 text-center"><FontAwesomeIcon icon={faBookOpen} className={"fas fa-book-open text-primary"}/>
            <span className="text-secondary">Book</span> List</h1>
        <p className="text-center">Add your book information to store it in database.</p>
         </>
        };
export default Header;