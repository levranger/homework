import {useState} from "react";
import "./Timer.css"
let interval;
let clickCounter = 0;
let secCounter = 0;
let waitCounter = 0;
let minCounter = 0;
const Timer = () => {
 const [secs,setSecs] = useState(0)
 const [mins,setMins] = useState(0)
 const [hours,setHours] = useState(0)
 const [btn,setBtn] = useState("start")
 const startTimer = () => {
     clickCounter++
     setBtn("stop");
        interval = setInterval(()=>{
            setSecs(secs=>secs+1)
            secCounter++
            if(secCounter>59){
                secCounter=0;
                setSecs(0);
                if(minCounter < 60){
                    setMins(mins=>mins+1);
                    minCounter++;
                } else {
                    minCounter = 0;
                    setMins(0)
                    setHours(hours=>hours+1)
                }
            
            }
        }, 10)
 }
 const Reset = () => {
    clearInterval(interval);
    setSecs(0);
    secCounter = 0;
    setMins(0);
    minCounter = 0;
    setHours(0)
 }
 const stopTimer = () => {
    
     clearInterval(interval)
     setBtn("start")
 }
 const Wait = () => {
     waitCounter++
     console.log(waitCounter);
     setTimeout(()=> {waitCounter===2?WaitClickCheck():waitCounter=0}, 300)
 }
 const WaitClickCheck = () => {
    clearInterval(interval)
 }

 return(
     <div className="timer">
         <p>{hours}:{mins}:{secs}</p>
        {btn==="start"?<button onClick={startTimer}>Start/Stop</button>:<button onClick={stopTimer}>Start/Stop</button> }
        <button onClick={Reset}>Reset</button>
        <button onClick={Wait}>Wait</button>
     </div>
  
 )
}

export default Timer;