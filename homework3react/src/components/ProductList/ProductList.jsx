import React from "react";

const ProductList = (props) => {
    const {dispatch,productItems} = props;
    return(
        <ul className="product-list">
            {productItems}
        </ul>
    )
};
export default ProductList;