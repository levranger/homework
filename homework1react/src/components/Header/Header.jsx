import React from "react";
import "./Header.css";
import Button from "../../shared/Button";
const Header = () => {
    return(
        <div className={'header'}>
            <div id="overlay">
            <div className={'upper-section'}>
               <div className={'upper-section-logo'}/>
               <div className={"upper-section-search-logo"} />
               <Button type={'signIn'} text={'Sign in'}/>
               <Button type={'signUp'} text={"Sign up"} />
            </div>
            <div className="lower-section">
                <div className="movie-description">
                <span className={"movie-label"}>THE JUNGLE BOOK</span>
                <span className={"movie-info"}>Adventure Drama Family Fantasy | 1h 46m</span>
                    <div className="movie-rating">
                        <img src="https://img.icons8.com/emoji/50/000000/star-emoji.png"/>
                        <img src="https://img.icons8.com/emoji/50/000000/star-emoji.png"/>
                        <img src="https://img.icons8.com/emoji/50/000000/star-emoji.png"/>
                        <img src="https://img.icons8.com/emoji/50/000000/star-emoji.png"/>
                        <img src="https://img.icons8.com/emoji/50/000000/star-emoji.png"/>
                        <div className="rating-score">4,8</div>
                    </div>
                </div>
                <div className="lower-btn-section">
                        <Button text={"Watch Now"} type={'watchNow'}/>
                        <Button text={"View Info"} type={'viewInfo'}/>
                        <span className={"favorites-label"}>Favorites</span>
                </div>
            </div>
            </div>
        </div>

    )
};
export default Header;