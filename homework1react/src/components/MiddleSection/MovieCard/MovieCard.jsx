import React, {Component} from "react";
import './MovieCard.css';

class MovieCard extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        const {text, img, genres, rating} = this.props;
    return <div className={"movie-card"}>
        <img src={img} className={"movie-img"}></img>
        <div className="movie-card-description">
            <span className="movie-text-description">{text}</span>
            <span className="movie-genres-description">{genres}</span>
            <div className="movie-rating-description">{rating}</div>
        </div>
    </div>
    }
}
export default MovieCard;