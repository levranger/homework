import React from "react";
import './MiddleSection.css'
import MovieCard from "./MovieCard/MovieCard"
import Image1 from "./movie-image1.png"
import Image2 from "./movie-image2.png"
import Image3 from "./movie-image3.png"
import Image4 from "./movie-image4.png"
import Image5 from "./movie-image5.png"
import Image6 from  "./movie-image6.png"
import Image7 from  "./movie-image7 (1).png"
import Image8 from "./movie-image8.png"
import Image9 from  "./movie-image9.png"
import Logo from "../../shared/Logo/index"
import LoadingIcon from "./Loading Icon@1X.png"
const MiddleSection = () => {
    return(
        <div className={"middle-section"}>
            <div className="upper-navbar">
                <span>Trending</span>
                <span>Top Rated</span>
                <span>New Arrivals</span>
                <span>Trailers</span>
                <span>Coming Soon</span>
                <span>Genre</span>
            </div>
            <div className="movie-container">
                <MovieCard img={Image1} text={"FANTASTIC BEASTS..."} genres={"Adventure, Family, Fantasy"} rating={"4,7"}/>
                <MovieCard img={Image2} text={"THE LEGEND OF TA..."} genres={"Action, Adventure, Fantasy"} rating={"4,2"}/>
                <MovieCard img={Image3} text={"FINDING DORY"} genres={"Adventure, Family, Fantasy"} rating={"4,7"}/>
                <MovieCard img={Image4} text={"THE BFG"} genres={"Adventure, Family, Fantasy"} rating={"3,2"}/>
                <MovieCard img={Image5} text={"INDEPENDENCE DAY"} genres={"Adventure, Family, Fantasy"} rating={"3,9"}/>
                <MovieCard img={Image6} text={"MOANA"} genres={"Action, Family, Fantasy"} rating={"3,2"}/>
                <MovieCard img={Image7} text={"DOCTOR STRANGE"} genres={"Adventure, Action, Fantasy"} rating={"4,8"}/>
                <MovieCard img={Image8} text={"NOW YOU SEE ME 2"} genres={"Adventure, Family, Action"} rating={"4,3"}/>
                <MovieCard img={Image9} text={"NINJA TURTLES"} genres={"Adventure, Family, Fantasy"} rating={"4,2"}/>
                <Logo src={LoadingIcon} alt={"Loading..."} className={"loading-icon"}/>

                
            </div>
        </div>
    )
};
export default MiddleSection