import React from 'react';
import './App.css';
import Header from "./components/Header/index"
import MiddleSection from './components/MiddleSection/index'
import Footer from "./components/Footer/index";
function App() {
  return (
    <div className="App">
      <Header/>
      <MiddleSection/>
      <Footer/>
    </div>
  );
}

export default App;
