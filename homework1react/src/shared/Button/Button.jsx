import React,{Component} from "react";

class Button extends Component{
    constructor(props) {
        super(props);


        this.buttonTypes = {
            'signIn': "sign-in-btn",
            'signUp': "sign-up-btn",
            'watchNow':"watch-now-btn",
            "viewInfo":"view-info-btn",
        };

    }


    render() {
        const {text, type} = this.props;
        let className = this.buttonTypes[type];
        return <button className={`btn ${className}`}>{text}</button>
    }
}

export default Button;