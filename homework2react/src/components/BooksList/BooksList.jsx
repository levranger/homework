import React,{Component} from "react";
import BookItem from "../BookItem/index"
class BooksList extends Component{


    render() {
        const {books, deleteBook, bookCounter, updateBook} = this.props;
        const bookItems = books.map(item=> {
            return <BookItem key={item.isbn} name={item.name} author={item.author} isbn={item.isbn} deleteBook={deleteBook} updateBook={updateBook}/>
        });
       return(
        <>
            <h3 id="book-count" className="book-count mt-5">Всего книг: {bookCounter}</h3>
            <table className="table table-striped mt-2">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN#</th>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody id="book-list">
                {bookItems}
                </tbody>
            </table>
        </>
       )
    }


}
export default BooksList;