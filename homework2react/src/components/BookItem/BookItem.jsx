import React,{Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";

export class BookItem extends Component {

    state={
        editMode:false,
        name:'',
        author:'',
        isbn:''
    };
    editSwitcher=(e)=>{
        this.setState({editMode:!this.state.editMode})
    };
    render() {
        const {name, author, isbn, id, deleteBook, updateBook} = this.props;
        let previousIsbn = isbn;
        if(!this.state.editMode){
        return(
            <tr data-id={id}>
                <td>{name}</td>
                <td>{author}</td>
                <td>{isbn}</td>
                <td><a href="#"  onClick={()=>this.editSwitcher(isbn)} className="btn btn-info btn-sm"><FontAwesomeIcon icon={faEdit} className="fas fa-edit"></FontAwesomeIcon></a></td>
                <td><a href="#" className="btn btn-danger btn-sm btn-delete" onClick={()=>deleteBook(isbn)}>X</a></td>
            </tr>
        )
    } else {
            const {name,author,isbn} = this.state;
            return (
               <>
                <form action="" id="update-book-form"> </form>
                    <tr>
                        <td>
                            <div className="form-group">
                                <input type="text" name="book-name" className="form-control" placeholder={name} onChange={(e)=>this.setState({name:e.target.value})} value={this.state.name}/>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <input type="text" name="book-author" className="form-control" placeholder={author} onChange={(e)=>this.setState({author:e.target.value})} value={this.state.author}/>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <input type="text" name="book-isbn" className="form-control" placeholder={isbn} onChange={(e)=>this.setState({isbn:e.target.value})} value={this.state.isbn}/>
                            </div>
                        </td>
                        <td><input type="submit" value="Update" className="btn btn-primary" onClick={(event)=>{this.editSwitcher(); updateBook(event,{name,author,isbn, previousIsbn})}}/></td>
                        <td></td>
                    </tr>
               </>
            )
        }
    }


}